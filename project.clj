(defproject centripetal-homework "0.0.1-SNAPSHOT"
  :description "Centripetal Homework" 
  :url "https://bitbucket.org/lambdaj/centripetal-homework/src/master/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [io.pedestal/pedestal.service "0.5.5"]
                 [io.pedestal/pedestal.jetty "0.5.5"]
                 [org.slf4j/jul-to-slf4j "1.7.25"]
                 [org.slf4j/jcl-over-slf4j "1.7.25"]
                 [org.apache.logging.log4j/log4j-slf4j-impl "2.11.1"]
                 [org.apache.logging.log4j/log4j-api "2.11.1"]
                 [org.apache.logging.log4j/log4j-core "2.11.1"]
                 [com.stuartsierra/component "0.4.0"]
                 [cheshire "5.8.1"]]
  :main ^:skip-aot centripetal-homework.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :resource-paths ["config", "resources"])

