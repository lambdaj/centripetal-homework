(ns centripetal-homework.server
  (:require [com.stuartsierra.component :as component]
            [io.pedestal.http :as http]
            [centripetal-homework.log :as log]))

(defn test?
  [service-map]
  (= :test (:env service-map)))

(defrecord Server [service-map service logger]
  component/Lifecycle
  (start [this]
    (log/trace logger "starting Server")
    (if service
      this                                                  ; makes the start fn idempotent by not initializing more than once
      (cond-> (:configs service-map)
              true http/create-server                       ; always create the server
              (not (test? service-map)) http/start          ; use this components test? fn to check if this is a test env
              true ((partial assoc this :service)))))       ; double check the necessity of the extra () surrounding (partial
  ; the (partial assoc this :service) is then applied to the previous threading result
  ; this also modifies this record
  (stop [this]
    (when (and service (not (test? service-map)))
      (log/trace logger "stopping Server")
      (http/stop service))
    (assoc this :service nil)))

(defn new-server []
  (map->Server {:logger (log/logger "Server")}))

(defrecord ServiceMap [routes configs env logger]
  component/Lifecycle
  (start [this]
    (log/trace logger "starting ServiceMap")
    (assoc this :configs {:env                     env
                          ::http/routes            (:routes routes)
                          ::http/type              :jetty
                          ::http/port              8080
                          ::http/container-options {:h2c? true
                                                    :h2?  false
                                                    :ssl? false}}))
  (stop [this]
    (log/trace logger "stopping ServiceMap")
    this))

(defn new-service-map [env]
  (map->ServiceMap {:env env
                    :logger (log/logger "ServiceMap")}))

