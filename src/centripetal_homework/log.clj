(ns centripetal-homework.log
  (:import (java.util Date Calendar)
           (org.apache.logging.log4j LogManager Logger)))

(defn logger [name] (. LogManager getLogger name))

(defn debug
  ([logger message] (. logger debug message))
  ([logger message ex] (. logger debug message ex)))

(defn info
  ([logger message] (. logger info message))
  ([logger message ex] (. logger info message ex)))

(defn trace
  ([logger message] (. logger trace message))
  ([logger message ex] (. logger trace message ex)))