(ns centripetal-homework.indicator
  (:require [com.stuartsierra.component :as component]
            [centripetal-homework.log :as log]
            [cheshire.core :as json]
            [clojure.java.io :as io]
            [centripetal-homework.db :as db]))

(defrecord IndicatorService [db logger]
  component/Lifecycle
  (start [this]
    (log/trace logger "starting IndicatorService")
    this)
  (stop [this]
    (log/trace logger "stopping IndicatorService")
    this))

(defn new-indicator-service []
  (map->IndicatorService {:logger (log/logger "IndicatorService")}))

(defn get-indicator [indicator-service id]
  (db/get-indicator (:db indicator-service) id))

