(ns centripetal-homework.db
  (:require [com.stuartsierra.component :as component]
            [centripetal-homework.log :as log]
            [cheshire.core :as json]
            [clojure.java.io :as io]))

(defrecord Db [data-loader data logger]
  component/Lifecycle
  (start [this]
    (log/trace logger "starting Db")
    (if data
      this
      (assoc this :data (data-loader))))

  (stop [this]
    (log/trace logger "stopping Db")
    this))

(defn get-indicator [db id]
  (get (:data db) id))

(defn new-db
  [{:keys [resource data]}]
  (map->Db {:logger      (log/logger "Db")
            :data-loader (cond
                           data (fn [] data)
                           resource (fn []
                                      (->> (json/parse-stream (io/reader (io/resource resource)) true)
                                           (mapcat #(get % :indicators))
                                           (reduce #(assoc %1 (str (:id %2)) %2) {})))
                           :else (fn [] {}))}))



