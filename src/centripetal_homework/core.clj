(ns centripetal-homework.core
  (:gen-class)
  (:require [centripetal-homework.server :as server]
            [centripetal-homework.routes :as routes]
            [centripetal-homework.indicator :as indicator]
            [centripetal-homework.db :as db]
            [com.stuartsierra.component :as component]))

(defn mk-system
  ([env] (mk-system env (db/new-db {:resource "indicators.json"})))
  ([env db-creator] (component/system-map
                      :db
                      db-creator

                      :indicator-service
                      (component/using
                        (indicator/new-indicator-service)
                        [:db])

                      :routes
                      (component/using
                        (routes/new-routes)
                        [:indicator-service])

                      :service-map
                      (component/using
                        (server/new-service-map env)
                        [:routes])

                      :server
                      (component/using
                        (server/new-server)
                        [:service-map]))))

(defn -main
  "for 'lein run'"
  [& args]
  (component/start (mk-system :prod)))
