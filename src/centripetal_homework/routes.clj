(ns centripetal-homework.routes
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.body-params :as body-params]
            [io.pedestal.http.route.definition.table :as table]
            [com.stuartsierra.component :as component]
            [centripetal-homework.indicator :as indicator]
            [centripetal-homework.log :as log]))

(defn get-indicator [indicator-service]
  (fn [request]
    (if-let [id (get-in request [:path-params :id])]
      (if-let [item (indicator/get-indicator indicator-service id)]
        {:status 200 :body item}
        {:status 404 :body (str "not found")})
      {:status 404 :body (str "not found")})))

(defrecord Routes [indicator-service routes logger]
  component/Lifecycle
  (start [this]
    (log/trace logger "starting Routes")
    (if routes
      this
      (assoc this :routes (table/table-routes
                            [["/indicators/:id" :get  (get-indicator indicator-service) :route-name :get-indicator]]))))
  (stop [this]
    (log/trace logger "stopping Routes")
    this))

(defn new-routes []
  (map->Routes {:logger (log/logger "Routes")}))