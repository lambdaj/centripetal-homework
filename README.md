## Getting Started

1. Start the application: `lein run`
2. Go to [localhost:8080](curl localhost:8080/indicators/87241653) to see a response containing an indicator
4. Run the tests with `lein test`
