(ns centripetal-homework.endpoint-test
  (:require [clojure.test :refer :all]
            [io.pedestal.test :refer :all]
            [io.pedestal.http :as http]
            [com.stuartsierra.component :as component]
            [centripetal-homework.db :as db]
            [centripetal-homework.indicator :as indicator]
            [centripetal-homework.routes :as routes]
            [centripetal-homework.server :as server]
            [centripetal-homework.core :as core]))

(def test-data
  {"1206236883"
   {:indicator   "crosscountry5k.com",
    :description "",
    :created     "2018-07-09T12:48:23",
    :title       "",
    :content     "",
    :type        "domain",
    :id          1206236883}})

(def service
  (let [system (component/start (core/mk-system :test (db/new-db {:data test-data})))]
    (get-in system [:server :service ::http/service-fn])))

(deftest indicator-endpoint-test
  (testing "success"
    (let [expected-body "{:indicator \"crosscountry5k.com\", :description \"\", :created \"2018-07-09T12:48:23\", :title \"\", :content \"\", :type \"domain\", :id 1206236883}"]
      (is (= expected-body (:body (response-for service :get "/indicators/1206236883"))))))
  (testing "failure"
    (is (= 404 (:status (response-for service :get "/indicators/notfound"))))))
