(ns centripetal-homework.db-test
  (:require [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [centripetal-homework.db :as db]))

(def test-data
  {"1206236883"
   {:indicator   "crosscountry5k.com",
    :description "",
    :created     "2018-07-09T12:48:23",
    :title       "",
    :content     "",
    :type        "domain",
    :id          1206236883}})

(deftest get-indicator
  (testing "failure"
    (let [instance (component/start (db/new-db {:data test-data}))]
      (is (nil? (db/get-indicator instance "not-found")))))
  (testing "success"
    (let [instance (component/start (db/new-db {:data test-data}))
          retrieved (db/get-indicator instance "1206236883")]
      (is (= "crosscountry5k.com" (:indicator retrieved))))))
